// Arithmetic operations
const mylib = {
    // Multiline arrow function
    add: (a, b) => {
        const sum = a + b;
        return sum;
    },
    // Multiline arrow function
    subtract: (a, b) => {
        return a - b;
    },
    // Singleline arrow function
    divide: (dividend, divisor) => {
        if (divisor === 0) {
            throw new Error('divisor 0 not allowed');
        }
        return dividend / divisor;
    },
    // Regular function
    multiply: function(a, b) {
        return a * b;
    }
};

module.exports = mylib;